package com.thuha.fragment.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.thuha.fragment.R;
import com.thuha.fragment.view.even.OnCallBack;

public class LoginFrg extends Fragment implements View.OnClickListener {
    public static final String TAG = LoginFrg.class.getName();
    public static final String KEY_SHOW_REGISTER_FRG = "KEY_SHOW_REGISTER_FRG" ;
    private Context mContext;
    private Button btRegister, btLogin;
    private EditText edtUserName, edtPass;
    private OnCallBack mCallBack;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frg_login, container, false);
        initViews(rootView);
        return rootView;
    }

    private void initViews(View rootView) {
        btRegister = rootView.findViewById(R.id.bt_register);
        btLogin = rootView.findViewById(R.id.bt_login);

        btLogin.setOnClickListener(this);
        btRegister.setOnClickListener(this);

        edtUserName = rootView.findViewById(R.id.edt_user_name);
        edtPass = rootView.findViewById(R.id.edt_pass);
    }

    @Override
    public void onStart() {
        Toast.makeText(mContext,edtUserName.getText().toString(),Toast.LENGTH_SHORT).show();
        super.onStart();
    }

    @Override
    public void onResume() {
        Toast.makeText(mContext,edtUserName.getText().toString(),Toast.LENGTH_SHORT).show();
        super.onResume();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_login:
                login();
                break;
            case R.id.bt_register:
                register();
                break;
            default:
                break;
        }
    }

    public void setmCallBack(OnCallBack callBack) {
        mCallBack = callBack;
    }

    private void register() {
        mCallBack.showFrg(KEY_SHOW_REGISTER_FRG, null);
    }

    private void login() {
        Toast.makeText(mContext, "Login", Toast.LENGTH_SHORT).show();
    }
}
