package com.thuha.fragment.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.thuha.fragment.R;
import com.thuha.fragment.view.even.OnCallBack;

public class RegisterFrg extends Fragment implements View.OnClickListener {
    public static final String TAG = RegisterFrg.class.getName();
    public static final String KEY_SHOW_LOGIN = "KEY_SHOW_LOGIN";
    private Context mContext;
    private OnCallBack mCallBack;
    private TextView tvLogin;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frg_register, container, false);
        initViews(rootView);
        return rootView;
    }

    private void initViews(View rootView) {
        tvLogin = rootView.findViewById(R.id.tv_back_login);
        tvLogin.setOnClickListener(this);

    }

    public void setmCallBack(OnCallBack callBack) {
        mCallBack = callBack;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_back_login) {
            mCallBack.showFrg(KEY_SHOW_LOGIN, null);
        }
    }
}
