package com.thuha.fragment.view.act;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.thuha.fragment.R;
import com.thuha.fragment.view.even.OnCallBack;
import com.thuha.fragment.view.fragment.LoginFrg;
import com.thuha.fragment.view.fragment.RegisterFrg;
import com.thuha.fragment.view.fragment.SplashFrg;

import java.util.List;

public class SplashActivity extends AppCompatActivity implements OnCallBack {
    private LoginFrg mLoginFrg;
    private FragmentManager frgMgr;
    private SplashFrg mSplashFrg;
    private RegisterFrg mRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
    }

    private void initViews() {
        mLoginFrg = new LoginFrg();
        mSplashFrg = new SplashFrg();
        mRegister = new RegisterFrg();

        mSplashFrg.setOnCallBack(this);
        mLoginFrg.setmCallBack(this);
        mRegister.setmCallBack(this);

        showFragment(mSplashFrg, SplashFrg.TAG);


    }

    private void showFragment(Fragment mFrg, String tag) {
        showFragment(mFrg, tag, false);
    }

    private void showFragment(Fragment mFrg, String tag, boolean isAdd) {
        frgMgr = getSupportFragmentManager();
        FragmentTransaction trans = frgMgr.beginTransaction().setCustomAnimations(R.animator.anim_alpha_in, R.animator.anim_alpha_out).replace(R.id.ln_main, mFrg, tag);
        if (isAdd) {
            trans.addToBackStack("backto");
            trans.commit();
        } else {
            trans.commit();

        }
    }

    @Override
    public void showFrg(String tag, Object obj) {
        if (tag.equals(SplashFrg.KEY_SHOW_LOGIN_FRG)) {
            showFragment(mLoginFrg, LoginFrg.TAG, false);
        } else if (tag.equals(LoginFrg.KEY_SHOW_REGISTER_FRG)) {
            addFrag(mRegister, RegisterFrg.TAG);
//            showFragment(mRegister, RegisterFrg.TAG, true);
        } else if (tag.equals(RegisterFrg.KEY_SHOW_LOGIN)) {
            showFragment(mLoginFrg, LoginFrg.TAG, false);

        }

    }

    private void addFrag(RegisterFrg mRegister, String tag) {
        frgMgr = getSupportFragmentManager();
        FragmentTransaction trans = frgMgr.beginTransaction().setCustomAnimations(R.animator.anim_alpha_in, R.animator.anim_alpha_out).add(R.id.ln_main, mRegister, tag).hide(mLoginFrg);
        trans.addToBackStack("hh");
        trans.commit();
    }

    public Fragment getFragment(String tag) {
        return getSupportFragmentManager().findFragmentByTag(tag);
    }

    public Fragment getCurrentFragment(String tag) {
        List<Fragment> listFrg = getSupportFragmentManager().getFragments();
        return listFrg.get(0);
    }
}
