package com.thuha.fragment.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.thuha.fragment.R;
import com.thuha.fragment.view.even.OnCallBack;

public class SplashFrg extends Fragment {
    public static final String TAG = SplashFrg.class.getName();
    public static final String KEY_SHOW_LOGIN_FRG = "KEY_SHOW_LOGIN_FRG";
    private Context mContext;
    private Button btRegister, btLogin;
    private EditText edtUserName, edtPass;
    private OnCallBack mCallBack;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frg_splash, container, false);
        initViews(rootView);
        return rootView;
    }

    private void initViews(View rootView) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showLoginFrg();
            }
        },2000);
    }

    public  void  setOnCallBack(OnCallBack mCallBack){
       this.mCallBack = mCallBack;
    }

    private void showLoginFrg() {
        mCallBack.showFrg(KEY_SHOW_LOGIN_FRG,null);
    }
}
